import pymongo
import os
import csv
import json
import time
import requests
import datetime
import random
import string
import urllib.parse
import decimal
import uuid
import re
import pymongo
from pymongo import MongoClient
from multiprocessing.dummy import Pool as ThreadPool
import concurrent.futures
import urllib.request
from random import shuffle
import spacy
import numpy as np
import pandas as pd
from bson.objectid import ObjectId
import pickle
from collections import defaultdict
import sys
import itertools
import matplotlib.pyplot as plt
import numpy as np
import os
import glob
import pandas as pd
from scipy.misc import imresize
import matplotlib.image as mpimg
from scipy import misc
from scipy.misc.pilutil import imresize
import numpy as np
from keras.applications.vgg16 import VGG16
from keras.layers import *
from keras.models import Model
from keras.preprocessing.image import load_img, img_to_array
from skimage import transform
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
# from keras.applications.inception_v3 import InceptionV3
# from keras.applications.inception50 import inception50
from keras.layers import Embedding
import requests
import urllib.request
from PIL import Image
from keras.models import load_model
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils
import numpy as np
from keras.applications.vgg16 import VGG16
from keras.applications.inception_v3 import InceptionV3
from keras.layers import *
from keras.models import Model
from keras.preprocessing.image import load_img, img_to_array
from skimage import transform
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Embedding
import io
from io import BytesIO
import tensorflow as tf
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.vgg19 import VGG19
from tensorflow.keras.applications.mobilenet import MobileNet
from tensorflow.keras.layers import Dropout, Flatten, Dense, Input, MaxPool2D, GlobalAveragePooling2D, Lambda, Conv2D, concatenate, ZeroPadding2D, Layer, MaxPooling2D
from tensorflow.keras.models import Model, load_model
from tensorflow.keras import backend as K
from keras.applications.resnet50 import ResNet50
# from utils import get_layers_output_by_name
import random
# from loss import *
from tensorflow.python.lib.io import file_io
import os
import zipfile

from tqdm import tqdm

def ranknet():
    vgg_model = VGG19(weights="imagenet", include_top=False, input_shape=(224,224,3))
    convnet_output = GlobalAveragePooling2D()(vgg_model.output)
    convnet_output = Dense(4096, activation='relu')(convnet_output)
    convnet_output = Dropout(0.5)(convnet_output)
    convnet_output = Dense(4096, activation='relu')(convnet_output)
    convnet_output = Dropout(0.5)(convnet_output)
    convnet_output = Lambda(lambda  x: K.l2_normalize(x,axis=1))(convnet_output)

    s1_inp = Input(shape=(224,224,3))
    s1 = MaxPool2D(pool_size=(4,4),strides = (4,4),padding='valid')(s1_inp)
    s1 = ZeroPadding2D(padding=(4, 4), data_format=None)(s1)
    s1 = Conv2D(96, kernel_size=(8, 8),strides=(4,4), padding='valid')(s1)
    s1 = ZeroPadding2D(padding=(2, 2), data_format=None)(s1)
    s1 = MaxPool2D(pool_size=(7,7),strides = (4,4),padding='valid')(s1)
    s1 = Flatten()(s1)

    s2_inp = Input(shape=(224,224,3))
    s2 = MaxPool2D(pool_size=(8,8),strides = (8,8),padding='valid')(s2_inp)
    s2 = ZeroPadding2D(padding=(4, 4), data_format=None)(s2)
    s2 = Conv2D(96, kernel_size=(8, 8),strides=(4,4), padding='valid')(s2)
    s2 = ZeroPadding2D(padding=(1, 1), data_format=None)(s2)
    s2 = MaxPool2D(pool_size=(3,3),strides = (2,2),padding='valid')(s2)
    s2 = Flatten()(s2)

    merge_one = concatenate([s1, s2])
    merge_one_norm = Lambda(lambda  x: K.l2_normalize(x,axis=1))(merge_one)
    merge_two = concatenate([merge_one_norm, convnet_output], axis=1)
    emb = Dense(4096)(merge_two)
    l2_norm_final = Lambda(lambda  x: K.l2_normalize(x,axis=1))(emb)

    final_model = tf.keras.models.Model(inputs=[s1_inp, s2_inp, vgg_model.input], outputs=l2_norm_final)

    return final_model


def load_model_vectorise_ranknet_1024(batch_posts,ranknet_model, pre_path="../../filestore/combined_products_2019_03_14"):
    ranknet_1024_posts = []
    # inception_model = inception_model_loaded
    # inception_model.load_weights("model/inception_deeprank_13.h5")
    for post in tqdm(batch_posts, desc='ranknet_1024'):
        # image_url = post["media"]["standard"][0]["url"]
        # print(image_url)
        try:
            # filepath = pre_path+"/"+str(post["_id"]).split('(')[1].split('"')[1]+".jpg"
            filepath = pre_path+"/"+str(post["_id"]).replace("ObjectId(\"",'').replace("\")",'')+".jpg"
            # img_response = requests.get(image_url)
            # image = Image.open(io.BytesIO(img_response.content))
            # if image.mode != "RGB":
            #     image = image.convert("RGB")
            # image = image.resize((224, 224))
            image = load_img(filepath)
            image = img_to_array(image).astype("float64")
            image = transform.resize(image, (224, 224))
            image *= 1. / 255
            image = np.expand_dims(image, axis = 0)
            embedding = ranknet_model.predict([image,image,image])[0]
        except OSError as ose:
            embedding = np.zeros((4096,), dtype=float)
        except:
            raise
            embedding = np.zeros((4096,), dtype=float)
        post['ranknet_vector'] = embedding.tolist()
        ranknet_1024_posts.append(post)


        # post_to_return = {}
        # # post_to_return = {"url": post["url"]}
        # # post_to_return['_id']=ObjectId(str(post["_id"]).replace("ObjectId(\"",'').replace("\")",''))
        # post_to_return['_id']=post["_id"]
        # post_to_return['inception_vector']=embedding.tolist()
        # # post_to_return[str(post["_id"]).replace("ObjectId(\"",'').replace("\")",'')] = embedding.tolist()
        # inception_1024_posts.append(post_to_return)
        # print(str(post_to_return["inception_vector"]))

    return ranknet_1024_posts

def delete_key(post, key):
    try:
        del post[key]
    except Exception as e:
        pass

def clean_post(post):
    new_post = post
    new_post["_id"] = post["_id"]# str(post["_id"]).replace("ObjectId(\"","").replace("\")","")

    try:
        image_url = post["media"]["standard"][0]["url"]
    except Exception as e:
        image_url = ""


    new_post["media"] = {"standard":[{"order":1,"url":image_url}]}


    if "description" in post:
        new_post["description_text"] = post["description_text"]
    else:
        new_post["description_text"] = ""

    if "description_text" in post:
        new_post["description_text"] = post["description_text"]
    else:
        new_post["description_text"] = ""

    if "name" in post:
        new_post["name"] = post["name"]
    else:
        new_post["name"] = ""

    return new_post

if __name__ == "__main__":
    WEBSITE_ID_HASH = {}
    # WEBSITE_ID_HASH["queens_cz"] = "5bf39a8bc9a7f60004dd8d04"
    # WEBSITE_ID_HASH["zoot_cz"] = "5bf39a6fc9a7f60004dd8d03"
    # WEBSITE_ID_HASH["footshop_cz"] = "5bf39aa9c9a7f60004dd8d05"
    # WEBSITE_ID_HASH["answear_cz"] = "5bf399f7c9a7f60004dd8d01"
    # WEBSITE_ID_HASH["aboutyou_cz"] = "5bf399d3c9a7f60004dd8d00"
    # WEBSITE_ID_HASH["zalando_cz"] = "5bf39a55c9a7f60004dd8d02"
    # WEBSITE_ID_HASH["freshlabels_cz"] = "5c38cbea0359f800041cdab0"

    WEBSITE_ID_HASH["hervis_at"] = "5ba20a59c423de434b232c36"
    WEBSITE_ID_HASH["xxlsports_at"] = "5ba20a93c423de434b232c37"
    WEBSITE_ID_HASH["decathlon_at"] = "5ba20abdc423de434b232c38"
    WEBSITE_ID_HASH["gigasport_at"] = "5ba20ae2c423de434b232c39"
    WEBSITE_ID_HASH["bergzeit_at"] = "5ba20b02c423de434b232c3a"
    WEBSITE_ID_HASH["blue_tomato_at"] = "5ba20b2cc423de434b232c3b"
    # WEBSITE_ID_HASH["adidas_us"] = "5c7fdad66c251b000443b910"
    # WEBSITE_ID_HASH["puma_us"] = "5c7fda916c251b000443b90e"
    # WEBSITE_ID_HASH["asos_gb"] = "5bc055046264490004432328"
    # WEBSITE_ID_HASH["debenhams_gb"] = "5c3cae7c29ecc300044c5f67"
    # WEBSITE_ID_HASH["marksandspencer_gb"] = "5c3caf2429ecc300044c5f69"
    ranknet_model_load = ranknet()
    ranknet_model_load.load_weights("model/weights-improvement-06-0.25.h5")
    website_id_hash = WEBSITE_ID_HASH
    # posts = []
    # new_posts = []

    MONGODB_URL1 = 'mongodb://root:MTyvE6ikos87@mongodb-dev.greendeck.co:27017/admin'
    client_prod = pymongo.MongoClient(
        MONGODB_URL1,
        ssl=False
    )
    MONGODB_URL2 = 'mongodb://root:MTyvE6ikos87@mongodb-dev.greendeck.co:27017/admin'
    client_dev = pymongo.MongoClient(
        MONGODB_URL2,
        ssl=False
    )
    db_prod = client_prod.black_widow_development
    db_dev = client_dev.faissal_dev
    collection = db_prod.hervis_combined_products
    new_collection = db_dev.hervis_ranknet_imagenet_vector_01_04_19
    for key, website_id in website_id_hash.items():
        print(key)
        limit = collection.find({'website_id':ObjectId(website_id)}).count()
        for batch in range(0,limit,100):
            posts = []
            new_posts = []
            try:
                cc = collection.find({'website_id':ObjectId(website_id)}).skip(batch).limit(100)
                for post in list(cc):
                    posts.append(post)
                    # clean post
                    post = clean_post(post)
                    new_posts.append(post)
                    # client.close()
            except Exception as e:
                raise
            print(str(len(new_posts)))
            return_post=load_model_vectorise_ranknet_1024(new_posts,ranknet_model_load)
            new_collection.insert_many(return_post)
        # client.close()
