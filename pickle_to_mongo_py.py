import sys
import os
import pickle
import os
import csv
import json
import time
import requests
import datetime
import random
import string
import urllib.parse
import decimal
import uuid
import re
import pymongo
from pymongo import MongoClient
from multiprocessing.dummy import Pool as ThreadPool
import concurrent.futures
import urllib.request
from random import shuffle
import spacy
import numpy as np
import pandas as pd
from bson.objectid import ObjectId
import pickle
from collections import defaultdict
import sys

def get_value_for_key(key, object):
    if key in object:
        return object[key]
    else:
        return ""

def get_pickle_length(pickle_file):
    try:
        fp = open(pickle_file, 'rb')
        data = pickle.load(fp)
        print(pickle_file+ ": pickle file length: "+str(len(data)))
        # print(data)
        fp.close()
        return len(data)
    except Exception as e:
        return 0


def get_pickle_list(pickle_file):
    try:
        fp = open(pickle_file, 'rb')
        data = pickle.load(fp)
        # print(pickle_file+ ": pickle file length: "+str(len(data)))
        # print(data)
        fp.close()
        return list(data)
    except Exception as e:
        raise


def clear_pickle(pickle_file):
    os.remove(pickle_file)

def main():
    print("asd1")
    MONGODB_URL1 = 'mongodb://admin:epxNOGMHaAiRRV5q@mongodb-prod.greendeck.co:27017/admin'
    client = pymongo.MongoClient(
        MONGODB_URL1,
        ssl=False
    )
    db = client.faissal
    # collection_resnet = db.cp_no_intu_resnet_1024_image_vectors
    # collection_inceptionv3 = db.cp_no_intu_inceptionv3_1024_image_vectors
    # collection_vgg19 = db.cp_no_intu_vgg19_1024_image_vectors
    collection_name_only = db.cp_name_description_768_vectors
    # combined_product_df_inception_vector.pkl  combined_product_df_resnet_vector.pkl  combined_product_df_vgg19_vector.pkl
    print("asd2")
    # data_resnet = pd.read_pickle("image_deepranking_models/combined_product_df_resnet_vector.pkl")
    # data_inceptionv3 = pd.read_pickle("image_deepranking_models/combined_product_df_inception_vector.pkl")
    # data_vgg19 = pd.read_pickle("image_deepranking_models/combined_product_df_vgg19_vector.pkl")
    data_name_only = pd.read_pickle("data/bert_with_name_description_second.pickle")
    print("asd3")

    # limit = len(data_resnet)
    # print("Dataframe Size: "+str(limit))
    # for batch in range(0,limit,20000):
    #     data_resnet_mini_df = data_resnet[batch:batch+20000]
    #     vector_list = list(data_resnet_mini_df["vector"])
    #     datas = []
    #     for i, _id in enumerate(data_resnet_mini_df["_id"]):
    #         object = {"_id":ObjectId(_id),"resnet_vector":vector_list[i]}
    #         datas.append(object)
    #     collection_resnet.insert_many(datas)
    #     print(str(collection_resnet.count_documents({})))

    # limit = len(data_inceptionv3)
    # print("Dataframe Size: "+str(limit))
    # for batch in range(0,limit,20000):
    #     data_inceptionv3_mini_df = data_inceptionv3[batch:batch+20000]
    #     vector_list = list(data_inceptionv3_mini_df["vector"])
    #     datas = []
    #     for i, _id in enumerate(data_inceptionv3_mini_df["_id"]):
    #         object = {"_id":ObjectId(_id),"resnet_vector":vector_list[i]}
    #         datas.append(object)
    #     collection_inceptionv3.insert_many(datas)
    #     print(str(collection_inceptionv3.count_documents({})))

    # limit = len(data_vgg19)
    # print("Dataframe Size: "+str(limit))
    # for batch in range(0,limit,20000):
    #     data_vgg19_mini_df = data_vgg19[batch:batch+20000]
    #     vector_list = list(data_vgg19_mini_df["vector"])
    #     datas = []
    #     for i, _id in enumerate(data_vgg19_mini_df["_id"]):
    #         object = {"_id":ObjectId(_id),"resnet_vector":vector_list[i]}
    #         datas.append(object)
    #     collection_vgg19.insert_many(datas)
    #     print(str(collection_vgg19.count_documents({})))

    limit = len(data_name_only)
    print("Dataframe Size: "+str(limit))
    for batch in range(0,limit,20000):
        data_name_only_mini = data_name_only[batch:batch+20000]
        datas = []
        for i in data_name_only_mini:
            object = {"_id":i['_id'].split('(')[1][0:-1],"name_vector":i['vector']}
            datas.append(object)
        collection_name_only.insert_many(datas)
        print(str(collection_name_only.count_documents({})))



    # for i,d in enumerate(data_resnet):
    #     datas.append(d)
    #     if (i+1)%1000 == 0:
    #         print("asd"+str(i))
    #         collection.insert_many(datas)
    #         datas = []


if __name__ == "__main__":
    main()
