import os
import json
import time
import requests
import datetime
import random
import csv
import string
import decimal
import uuid
# from selenium import webdriver
import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId
from multiprocessing.dummy import Pool as ThreadPool
from tqdm import tqdm


def actually_move(data):
    id_temp = data['_id']
    data_2 = list(source_collection_2.find({"_id":id_temp}))
    data_3 = list(source_collection_3.find({"_id":id_temp}))
    data_4 = list(source_collection_4.find({"_id":id_temp}))
    data['resnet_vector'] = data_2[0]['ir_vector']
    data['inception_vector'] = data_3[0]['ir_vector']
    data['vgg_vector'] = data_4[0]['ir_vector']
    # data.pop('resnet_vector')
    # data.pop('inception_vector')
    # data.pop('vgg_vector')
    return data


if __name__ == '__main__':
    print("starting your transfer")

    MONGODB_URL1 = 'mongodb://root:MTyvE6ikos87@mongodb-dev.greendeck.co:27017/admin'
    client_dev = pymongo.MongoClient(
       MONGODB_URL1,
       ssl=False)
    # MONGODB_URL2 = 'mongodb://admin:epxNOGMHaAiRRV5q@mongodb-prod.greendeck.co:27017/admin'
    # client_prod = pymongo.MongoClient(
    #     MONGODB_URL2,
    #     ssl=False)


    source_db = client_dev['black_widow_development']
    sink_db = client_dev['faissal_dev']
    source_collection_1 = source_db['hervis_combined_products_vec']
    source_collection_2 = sink_db['hervis_ranknet_resnet_2019_04_08']
    source_collection_3 = sink_db['hervis_ranknet_inception_2019_04_08']
    source_collection_4 = sink_db['hervis_ranknet_vgg19_2019_04_08']
    sink_collection = sink_db['hervis_3ir_2nlp_ranknet_2019_04_09']
    print('hi')
    # xxx = range(0,source_collection.count_documents({}),100)
    xxx = range(0,200396,50)
    print(str(xxx))
    for batch in tqdm(xxx):
        try:
            results = []
            int(batch)
            # pool = ThreadPool(10)
            list_view_objects = source_collection_1.find({}).sort("_id", 1).skip(batch).limit(50)
            print(batch)
            # results = pool.map(actually_move, list_view_objects)
            # pool.close()
            # pool.join()
            for post in list(list_view_objects):
                return_post = actually_move(post)
                results.append(return_post)
            try:
                sink_collection.insert_many(results)
                print("YOLO: "+str(sink_collection.count()))
            except:
                raise
                print("YOLO: "+str(sink_collection.count()))
        except Exception as e:
            raise
            print("failed: "+ str(e))
