import json
import os
import sys
import io
import requests
from io import BytesIO
import json
import time
import requests
import datetime
import random
import string
import urllib.parse
import decimal
import uuid
import re
import pymongo
from pymongo import MongoClient
from multiprocessing.dummy import Pool as ThreadPool
import concurrent.futures
import urllib.request
import sys, os, multiprocessing, urllib3, csv
from PIL import Image
from io import BytesIO
from tqdm  import tqdm
import json
import pandas

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

client = urllib3.PoolManager(500)

def get_path(post):
    # set location of the downloaded images
    p1 = post['meta']['label']

    path = ""
    if p1 == "":
        return path
    else:
        path += p1 + "/"
        return path

def DownloadImage(post):
  # get URL of the downloaded images
  out_dir = "combined_products_2019_03_14"
  try:
    url = post['media']['standard'][0]['url']
  except:
    return
  # url = url[:url.find('?')]
  # url = url.replace("https:","http:")
  # url = url[:url.find('?')]
  # url = url.replace("https:","http:")

  if url.startswith("http:www"):
      url = url.replace("http:www", "http://www")
  if url.startswith("https:www"):
      url = url.replace("https:www", "https://www")
  if not (url.startswith("https://") or url.startswith("http://")):
      url = "https://"+url

  url = url.replace("///","//")

  url = url.replace("?resize=1200,0","?resize=400,0")
  url = url.replace("?resize=1400,0","?resize=400,0")
  url = url.replace("?resize=1500,0","?resize=400,0")
  url = url.replace("?resize=0,1200","?resize=0,400")
  url = url.replace("?resize=0,1400","?resize=0,400")
  url = url.replace("?resize=0,1500","?resize=0,400")
  url = url.replace("?resize=1400,1400","?resize=400,400")
  url = url.replace("?resize=1500,1500","?resize=400,400")
  url = url.replace("?resize=1200,1200","?resize=400,400")

  key = str(post['_id']).replace("\"","").replace("(","").replace("ObjectId","")

  subdirectory = out_dir+"/"
  filename = subdirectory+key+".jpg"

  try:
      if not os.path.exists(subdirectory):
        os.makedirs(subdirectory)
  except Exception as e:
      print('Location: %s already exists.' % subdirectory)
      return


  if os.path.exists(filename):
    print('Image %s already exists. Skipping download.' % filename)
    return

  try:
    # global client
    # response = client.request('GET', url)#, timeout=30)
    # image_data = response.data
    response = requests.get(url,verify = False,timeout = 10)
    image_data = response.content
  except:
    print('Warning: Could not download image %s from %s' % (key, url))
    return

  try:
    pil_image = Image.open(BytesIO(image_data))
  except:
    print('Warning: Failed to parse image %s %s' % (key,url))
    return

  try:
    pil_image = pil_image.resize((400, 400),Image.ANTIALIAS)
  except:
    print('Warning: Failed to resize image %s %s' % (key,url))
    return

  try:
    pil_image_rgb = pil_image.convert('RGB')
  except:
    print('Warning: Failed to convert image %s to RGB' % key)
    return

  try:
    pil_image_rgb.save(filename, format='JPEG', quality=90)
    # print('Success: Proceeding to save image %s' % filename)
  except:
    print('Warning: Failed to save image %s' % filename)
    return


def main2():
    for batch in range(0,753510,1000):
    # for batch in range(50000,100000,2000):
    # for batch in range(100000,150000,2000):
    # for batch in range(150000,194985,2000):
        try:
            # MONGODB_URL1 = 'mongodb://root:MTyvE6ikos87@mongodb-dev.greendeck.co:27017/admin'
            # client = pymongo.MongoClient(
            #     MONGODB_URL1,
            #     ssl=False
            # )
            MONGODB_URL1 = 'mongodb://admin:epxNOGMHaAiRRV5q@mongodb-prod.greendeck.co:27017/admin'
            client = pymongo.MongoClient(
            MONGODB_URL1,
            ssl=False)
            db = client["octopus_fashion"]
            collection = db["combined_products"]
            # new_pdp_collection = db.women_result
            int(batch)
            pool = ThreadPool(100)
            list_view_objects = collection.find({}).sort("_id", 1).skip(batch).limit(1000)
            print(batch)
            pool.map(DownloadImage, list_view_objects)
            pool.close()
            pool.join()
            # new_pdp_collection.insert(results)
            # print("YOLO")
            # print(str(new_pdp_collection.count()))
            # client.close()
        except Exception as e:
            raise
            # print("failed")
            with open('intu_details_errors.csv', 'a') as errorFile:
                errorWriter = csv.writer(errorFile)
                errorWriter.writerow([batch, str(e)])


if __name__ == '__main__':
    main2()
