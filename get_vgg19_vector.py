import pymongo
import os
import csv
import json
import time
import requests
import datetime
import random
import string
import urllib.parse
import decimal
import uuid
import re
import pymongo
from pymongo import MongoClient
from multiprocessing.dummy import Pool as ThreadPool
import concurrent.futures
import urllib.request
from random import shuffle
import spacy
import numpy as np
import pandas as pd
from bson.objectid import ObjectId
import pickle
from collections import defaultdict
import sys
import itertools
import matplotlib.pyplot as plt
import numpy as np
import os
import glob
import pandas as pd
from scipy.misc import imresize
import matplotlib.image as mpimg
from scipy import misc
from scipy.misc.pilutil import imresize
import numpy as np
from keras.applications.vgg16 import VGG16
from keras.applications.vgg19 import VGG19
from keras.layers import *
from keras.models import Model
from keras.preprocessing.image import load_img, img_to_array
from skimage import transform
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
# from keras.applications.inception_v3 import InceptionV3
# from keras.applications.vgg50 import vgg50
from keras.layers import Embedding
import requests
import urllib.request
from PIL import Image
from keras.models import load_model
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils
import numpy as np
# from keras.applications.inception_v3 import InceptionV3
from keras.layers import *
from keras.models import Model
from keras.preprocessing.image import load_img, img_to_array
from skimage import transform
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Embedding
import io
from io import BytesIO
from multiprocessing.dummy import Pool as ThreadPool
from tqdm import tqdm
import tensorflow as tf

def convnet_model_(first_input):
    vgg_model = VGG19(weights='imagenet', include_top=False)
    vgg_model.layers.pop(0)
    newInput = first_input   # let us say this new InputLayer
    newOutputs = vgg_model(newInput)
    newModel = Model(newInput, newOutputs)
    x = newModel.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024, activation='relu')(x)
    x = Dropout(0.6)(x)
    x = Dense(1024, activation='relu')(x)
    x = Dropout(0.6)(x)
    x = Lambda(lambda  x_: K.l2_normalize(x,axis=1))(x)
    convnet_model = Model(inputs=newInput, outputs=x)
    return convnet_model

def deep_rank_model():
    first_input = Input(shape=(224,224,3))
    print(first_input)
    convnet_model = convnet_model_(first_input)

    first_conv = Conv2D(96, kernel_size=(8, 8),strides=(16,16), padding='same')(first_input)
    first_max = MaxPool2D(pool_size=(3,3),strides = (4,4),padding='same')(first_conv)
    first_max = Flatten()(first_max)
    first_max = Lambda(lambda  x: K.l2_normalize(x,axis=1))(first_max)

    second_input = first_input
    second_conv = Conv2D(96, kernel_size=(8, 8),strides=(32,32), padding='same')(second_input)
    second_max = MaxPool2D(pool_size=(7,7),strides = (2,2),padding='same')(second_conv)
    second_max = Flatten()(second_max)
    second_max = Lambda(lambda  x: K.l2_normalize(x,axis=1))(second_max)

    merge_one = concatenate([first_max, second_max])

    merge_two = concatenate([merge_one, convnet_model.output])
    emb = Dense(1024)(merge_two)
    l2_norm_final = Lambda(lambda  x: K.l2_normalize(x,axis=1))(emb)

    final_model = Model(inputs=first_input, outputs=l2_norm_final)

    return final_model



def load_model_vectorise_vgg_1024(batch_posts,pre_path="/home/ubuntu/filestore/combined_products_2019_03_14"):
    vgg_1024_posts = []
    for post in tqdm(batch_posts, desc='vgg_1024'):
        try:
            filepath = pre_path+"/"+str(post["_id"]).replace("ObjectId(\"",'').replace("\")",'')+".jpg"
            image = load_img(filepath)
            image = img_to_array(image).astype("float64")
            image = transform.resize(image, (224, 224))
            image *= 1. / 255
            image = np.expand_dims(image, axis = 0)
            with graph.as_default():
                embedding = vgg_model_loaded.predict(image)[0]
        except OSError as ose:
            embedding = np.zeros((1024,), dtype=float)
        except:
            raise
            embedding = np.zeros((1024,), dtype=float)
        post['vgg_vector'] = embedding.tolist()
        vgg_1024_posts.append(post)
    return vgg_1024_posts

def delete_key(post, key):
    try:
        del post[key]
    except Exception as e:
        pass

def clean_post(post):
    new_post = post
    new_post["_id"] = post["_id"]# str(post["_id"]).replace("ObjectId(\"","").replace("\")","")

    try:
        image_url = post["media"]["standard"][0]["url"]
    except Exception as e:
        image_url = ""


    new_post["media"] = {"standard":[{"order":1,"url":image_url}]}


    if "description" in post:
        new_post["description_text"] = post["description_text"]
    else:
        new_post["description_text"] = ""

    if "description_text" in post:
        new_post["description_text"] = post["description_text"]
    else:
        new_post["description_text"] = ""

    if "name" in post:
        new_post["name"] = post["name"]
    else:
        new_post["name"] = ""

    return new_post

if __name__ == "__main__":
    WEBSITE_ID_HASH = {}
    # WEBSITE_ID_HASH["queens_cz"] = "5bf39a8bc9a7f60004dd8d04"
    # WEBSITE_ID_HASH["zoot_cz"] = "5bf39a6fc9a7f60004dd8d03"
    # WEBSITE_ID_HASH["footshop_cz"] = "5bf39aa9c9a7f60004dd8d05"
    # WEBSITE_ID_HASH["answear_cz"] = "5bf399f7c9a7f60004dd8d01"
    # WEBSITE_ID_HASH["aboutyou_cz"] = "5bf399d3c9a7f60004dd8d00"
    # WEBSITE_ID_HASH["zalando_cz"] = "5bf39a55c9a7f60004dd8d02"
    # WEBSITE_ID_HASH["freshlabels_cz"] = "5c38cbea0359f800041cdab0"

    WEBSITE_ID_HASH["hervis_at"] = "5ba20a59c423de434b232c36"
    WEBSITE_ID_HASH["xxlsports_at"] = "5ba20a93c423de434b232c37"
    WEBSITE_ID_HASH["decathlon_at"] = "5ba20abdc423de434b232c38"
    WEBSITE_ID_HASH["gigasport_at"] = "5ba20ae2c423de434b232c39"
    WEBSITE_ID_HASH["bergzeit_at"] = "5ba20b02c423de434b232c3a"
    WEBSITE_ID_HASH["blue_tomato_at"] = "5ba20b2cc423de434b232c3b"
    # WEBSITE_ID_HASH["adidas_us"] = "5c7fdad66c251b000443b910"
    # WEBSITE_ID_HASH["puma_us"] = "5c7fda916c251b000443b90e"
    # WEBSITE_ID_HASH["asos_gb"] = "5bc055046264490004432328"
    # WEBSITE_ID_HASH["debenhams_gb"] = "5c3cae7c29ecc300044c5f67"
    # WEBSITE_ID_HASH["marksandspencer_gb"] = "5c3caf2429ecc300044c5f69"
    global vgg_model_loaded
    vgg_model_loaded = deep_rank_model()
    vgg_model_loaded.load_weights("model/vgg19_deeprank_13.h5")
    print('=================================')
    print('model loaded')
    global graph
    graph = tf.get_default_graph()
    website_id_hash = WEBSITE_ID_HASH

    MONGODB_URL2 = 'mongodb://root:MTyvE6ikos87@mongodb-dev.greendeck.co:27017/admin'
    client_dev = pymongo.MongoClient(
        MONGODB_URL2,
        ssl=False
    )
    source_db = client_dev['faissal_dev']
    source_collection = source_db['hervis_2IR_name_desc_vector_03_04_19']
    sink_collection = source_db['hervis_3IR_imagenet_name_desc_vector_2019_04_10']

    xxx = range(0,200396,100)
    print(str(xxx))
    for batch in xxx:
        try:
            posts=[]
            int(batch)
            list_view_objects = source_collection.find({}).sort("_id", 1).skip(batch).limit(100)
            print(batch)
            for post in list(list_view_objects):
                posts.append(post)
            results = load_model_vectorise_vgg_1024(posts)
            try:
                sink_collection.insert_many(results)
                print("YOLO: "+str(sink_collection.count()))
            except:
                print("YOLO: "+str(sink_collection.count()))
                raise
        except Exception as e:
            raise
            print("failed: "+ str(e))



    # xxx = range(0,200396,200)
    # print(str(xxx))
    # for batch in tqdm(xxx):
    #     try:
    #         int(batch)
    #         pool = ThreadPool(10)
    #         list_view_objects = source_collection.find({}).sort("_id", 1).skip(batch).limit(200)
    #         print(batch)
    #         # posts=[]
    #         # for post in list(list_view_objects):
    #         #     posts.append(post)
    #         results = pool.map(load_model_vectorise_vgg_1024,list_view_objects)
    #         try:
    #             sink_collection.insert_many(results)
    #             print("YOLO: "+str(sink_collection.count()))
    #         except:
    #             print("YOLO: "+str(sink_collection.count()))
    #             raise
    #     except Exception as e:
    #         raise
    #         print("failed: "+ str(e))
